obj-m += metanite.o 
run:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	sudo insmod metanite.ko
	sleep 3s
	sudo rmmod metanite
	sudo dmesg -c
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
