#	cosc439-001-project1: MetaNite

# Objective
MetaNite will categorize each user process based on memory usage and will set priotiries of processes under its categorizaiton scheme. The process with the highest memory allocation will be given the higest priority, while others with the lowest memory allocaiton will be given the lowest priority of completion.

This was tested on Ubuntu 20.04 with kernel 5.8.0-50
## Requirements
- Install linux headers
- gcc
- download the gitlab repo
## Usage
To run and setup the module type <br />
`$ make ` <br />
to clean the module type <br />
`$ make clean`


### Video Example
#### Make
![watch gif](https://gitlab.com/the-group-8-collective/cosc439-001-project1/-/raw/master/make.gif)
#### Clean
![watch gif](https://gitlab.com/the-group-8-collective/cosc439-001-project1/-/raw/master/clean.gif)

### References
- Admin. (2018, March 5). Linux Kernel Tutorial for Beginners. Linuxhint. Retrieved March 5, 2021, from https://linuxhint.com/linux-kernel-tutorial-beginners/
- EverythingisBlue. (2019, May 27). Top Command In Linux with Examples.GeeksforGeeks. Retrieved March 5, 2021, from https://www.geeksforgeeks.org/top-command-in-linux-with-examples/
- Helin, E. Renberg, A. (2015, January 19). The little book about OS development. CreativeCommons. Retrieved March 5, 2021, from http://littleosbook.github.io/
- IBM. (2014). Advanced Accounting subsystem. Retrieved March 3, 2021, from https://www.ibm.com/support/knowledgecenter/ssw_aix_71/accounting/accounting_pdf.pdf
- Kerrisk, M. (2020, December 12). Top(1) - Linux manual page. Linux man-pages project. Retrieved March 5, 2021, from https://man7.org/linux/man-pages/man1/top.1.html
- Silberschatz, A. Galvin, P. Gagne, G. (2013).  Operating System Concepts Essentials 2nd Edition. Wiley. Retrieved March 5, 2021, from https://www.wiley.com/en-us/Operating+System+Concepts+Essentials%2C+2nd+Edition-p-9781118804926
