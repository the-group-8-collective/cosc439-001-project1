/********************************
 * File created by Group Eight Collective (G8C)
 * G8C Members: Kordell Hutchins, Kobe Luong
 * April 30th, 2021
 * Professor Tessler
 * COSC 439 001
 * Program Name: MetaNite
 ********************************
 */
#include </usr/include/asm-generic/unistd.h>		//execvp:
#include <linux/kernel.h>      // printk(), pr_*()
#include <linux/module.h>      // THIS_MODULE, MODULE_VERSION, ...
#include <linux/init.h>        // module_{init,exit}
#include <linux/sched/task.h>  // struct task_struct, {get,put}_task_struct()
#include <linux/mm.h>          // get_mm_rss()
#include <linux/pid.h>         // struct pid, get_pid_task(), find_get_pid()
#include <linux/moduleparam.h> // module_param_named()
#include <linux/slab.h>		//krealloc()
#include <linux/sched.h>
struct task_struct *task;        /*    Structure defined in sched.h for tasks/processes    */
struct task_struct *p;
typedef struct 
{
  int *array;
  size_t used;
  size_t size;
} id_array;

typedef struct 
{
  unsigned long *array;
  size_t used;
  size_t size;
} ram_array;

int rss = 0;
int counter = 0;//Iterator for arrays
//Change according to available memory size
int highLimit =322122547; //3GB
int mediumLimit=214748364;//2GB
int lowLimit= 107374182;//1GB

int y = 0;//Used to store unmodified priority of process

//The RAM usage and process ID data types are different and require different argument types
void initArray(id_array *a, size_t initialSize);
void insertArray(id_array *a, int element);
void initarray(ram_array *a, size_t initialSize);
void insertarray(ram_array *a, long unsigned element);

 // Function to swap the the position of two elements
void swapper(long unsigned *a, long unsigned *b);
  
void id_swap(int *a, int *b);//Swap IDs when sorting
  
void heapify(unsigned long* arr, int n, int i, int *id);
  
  // Main function to do heap sort
void heapSort(ram_array *a, id_array *b);

id_array pid;
ram_array ram;

int iterate_init(void)                    /*    Init Module    */
{

    printk(KERN_INFO "%s","LOADING MODULE\n");    /*    good practice to log when loading/removing modules    */
     

    
    initArray(&pid, 1);
    initarray(&ram, 1);
    for_each_process( task ) //Loop through all the processes to count how many there are. This function is located in linux\sched\signal.h
    {
    	get_task_struct(task);
	    insertArray(&pid, task->pid);   
	    if(task->mm)
	    {
	    	rss = get_mm_rss(task->mm) << PAGE_SHIFT;
		insertarray(&ram, rss);
	    }
	    else
	    {
	    	insertarray(&ram, 0);
	    }         
	    counter++;
	    put_task_struct(task);
    }    
	counter = 0;
	//Special heapSort that sorts ram in descending order and changes corresponding pid to match.
	heapSort(&ram, &pid);
	while(counter < ram.used)
	{
		p = pid_task(find_vpid(pid.array[counter]), PIDTYPE_PID);//task structure of related p_id
		y = p->prio;//priority of the process
    		   //If you want to see the process name, it's a string in p. Use p->comm.
		printk("Process %i, %s, %lu bytes of RAM, Current Priority: %i\n", pid.array[counter], p->comm, ram.array[counter], y);//Remember to put a \n at the end of each printk call, or your formatting can get messed up.
/*
 * Power Services: 80-51%
 * renice -n 5 -p [pid] //(20-5)/20 = 75% of CPU
 * Standard Services: 50-26%
 * renice -n 10 -p [pid] //(20-10)/20 = 50% of CPU
 * Optimized Services: 1-25%
 * renice -n 15 -p [pid] //(20-15)/20 = 25% of CPU
 */
			//Modifying Priority Engine	 
			if (ram.array[counter] < mediumLimit){
	      set_user_nice(p, 15);
				printk("Process %i is a Optimized Process!! It's new priority is %i\n", pid.array[counter], p->prio);
				//int which =PRIO_PROCESS;
				//int ret;
				//ret = setpriority(which, pid.array[counter], 15);
			}
			else if (ram.array[counter] >= mediumLimit && ram.array[counter] <= highLimit){
	      set_user_nice(p, 10);
				printk("Process %i is a Standard Process!!\n It's new priority is %i\n", pid.array[counter], p->prio);
			}
			else {
	      set_user_nice(p, 5);
				printk("Process %i is a Power Process!!\n It's new priority is %i\n", pid.array[counter], p->prio);

			}
			counter++;
			
		
	
	}
	
    return 0;
 
}                /*    End of Init Module    */



 // Function to swap the the position of two elements
void swapper(long unsigned *a, long unsigned *b) 
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
  
void id_swap(int *a, int *b) 
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
  
void heapify(unsigned long* arr, int n, int i, int *id) 
{
    // Find smallest among root, left child and right child
    int smallest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;
  
    if (left < n && arr[left] < arr[smallest])
      smallest = left;
  
    if (right < n && arr[right] < arr[smallest])
      smallest = right;
  
    // Swap and continue heapifying if root is not largest
    if (smallest != i) {
      swapper(&arr[i], &arr[smallest]);
      id_swap(&id[i], &id[smallest]);//Switch corresponding ID with its RAM usage.
      heapify(arr, n, smallest, id);
    }
}
  
  // Main function to do heap sort
void heapSort(ram_array *a, id_array *b) 
{
    // Build max heap
    int i = a->used / 2 - 1;
    while(i >= 0)
    {
    	heapify(a->array, a->used, i, b->array);
    	i--;
    }

	i = a->used - 1;  
    // Heap sort
    while(i >= 0)
    {
    	swapper(&a->array[0], &a->array[i]);
    	id_swap(&b->array[0], &b->array[i]);//Switch corresponding ID with its RAM usage.
    	// Heapify root element to get highest element at root again
    	heapify(a->array, i, 0, b->array);
    	i--;
    }

}



void initArray(id_array *a, size_t initialSize) 
{
  a->array = kvmalloc(initialSize * sizeof(int), GFP_KERNEL);
  a->used = 0;
  a->size = initialSize;
}

void insertArray(id_array *a, int element) 
{
  // a->used is the number of used entries, because a->array[a->used++] updates a->used only *after* the array has been accessed.
  // Therefore a->used can go up to a->size 
  if (a->used == a->size) {
    a->size *= 2;
    a->array = krealloc(a->array, a->size * sizeof(int), GFP_KERNEL);
  }
  a->array[a->used++] = element;
}

     
void initarray(ram_array *a, size_t initialSize) 
{
  a->array = kvmalloc(initialSize * sizeof(unsigned long), GFP_KERNEL);
  a->used = 0;
  a->size = initialSize;
}

void insertarray(ram_array *a, unsigned long element) 
{
  // a->used is the number of used entries, because a->array[a->used++] updates a->used only *after* the array has been accessed.
  // Therefore a->used can go up to a->size 
  if (a->used == a->size) {
    a->size *= 2;
    a->array = krealloc(a->array, a->size * sizeof(unsigned long), GFP_KERNEL);
  }
  a->array[a->used++] = element;
}    
     
     
   
     
     
void cleanup_exit(void)        /*    Exit Module    */
{
 
 
    printk(KERN_INFO "%s","REMOVING MODULE\n");
 
}                /*    End of Exit Module    */


module_init(iterate_init);    /*    Load Module MACRO    */
module_exit(cleanup_exit);    /*    Remove Module MACRO    */
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("ITERATE THROUGH ALL PROCESSES/CHILD PROCESSES IN THE OS");
MODULE_AUTHOR("G8C: Group 8 Collective");
